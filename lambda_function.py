import boto3
from boto3.dynamodb.conditions import Key, Attr
import pandas as pd
from time_util import TimeUtil
from collections import defaultdict

def lambda_handler(event, context):
    t = TimeUtil()
    start_date = t.get_start_utc_timestamp(1)
    end_date = t.get_end_utc_timestamp(1)
    t_name = 'rsp-do-dispatch-offer-activity'
    facility_id = '32033201'
    data = fetch_data(t_name, facility_id, start_date, end_date)
    a_data = aggregate_data(data)
    print(a_data)
    output = defaultdict(dict)
    for d in a_data:
        output[d['facilityId']].update(d)
    print(output.values())
    insert_item('test-aggregate', output.values())

def fetch_data(t_name, facility_id, start_date, end_date):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(t_name)
    response = table.scan(
	FilterExpression=Attr('eventUtcDTime').between(start_date, end_date)
	)
    data = []
    if len(response['Items']) > 0:
        for i in response['Items']:   
    	    data.append(i)
        while 'LastEvaluatedKey' in response:   
    	    response = table.query(KeyConditionExpression=Key('eventUtcDTime').between(start_date, end_date) &Key('facilityId').eq(facility_id), ExclusiveStartKey=response['LastEvaluatedKey'])
    	    for i in response['Items']:
    	        data.append(i)
            
    return data

def aggregate_data(datas):
    df = pd.DataFrame(data=datas)
    group_data = df.groupby( [ "facilityId", "offerEvent"] )
    res = group_data.count().to_dict('index')
    lst_response = []
    for key, value in res.iteritems():
	    response = {}
	    response['facilityId'] = key[0]
	    response[key[1]] = value.get('insertUtcDTime')
	    lst_response.append(response)	
    return lst_response

def insert_item(t_name, items):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(t_name)
    #table.put_item(Item=item)
    with table.batch_writer() as batch:
	for item in items:
	    batch.put_item(Item=item)

lambda_handler('','')
